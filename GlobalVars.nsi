;--------------------------------
; Global Variables
  ; DataDir contains the directory where the datafiles will be installed
  var DataDir
  var MaxAnalyticsEnabled
  var Max1Enabled 
  var Max2Enabled
  var NISTLibraryEnabled
  var QTAViewerEnabled
  var LicenseFile
  
  var InstallAnalytics
  var InstallMax1
  var InstallMax2
  var InstallQTAViewer

  var InstallDeskTopShortCuts


  Function InitializeGlobals
    StrCpy $InstallDeskTopShortCuts "false"
    StrCpy $Max1Enabled "true"
    StrCpy $Max2Enabled "true"
    StrCpy $NISTLibraryEnabled "true"
    StrCpy $MaxAnalyticsEnabled "true"
    StrCpy $QTAViewerEnabled "true"

    StrCpy $InstallAnalytics "false"
    StrCpy $InstallMax1 "false"
    StrCpy $InstallMax2 "false"
    StrCpy $InstallQTAViewer "false"


    InstallerHelper::DefaultLicenseFilePath
    Pop $LicenseFile

  FunctionEnd