!include nsDialogs.nsh
!include LogicLib.nsh
!include WinCore.nsh ; MAKELONG
!include FileFunc.nsh

!pragma warning disable 8000 ; "Page instfiles not used, no sections will be executed!"

!define LSUnknown  -1
!define LSValid  0
!define LSLicenseFileNotFound  1
!define LSInvalidKey 2
!define LSInvalidSoftwareVersion 3
!define LSInvalidSerialNumber 4
!define LSExpiredClose 5
!define LSExpired 6


Var LicenseDialog
var LDLabel
var LDBrowseButton
var LDRefreshButton
var LDFileEdit
var LDNextButton
var LDStatus

SetPluginUnload alwaysoff

function LicensePromptPage
    nsDialogs::Create 1018
    Pop $LicenseDialog

	GetDlgItem $LDNextButton $LicenseDialog 1 ; next=1, cancel=2, back=315

    ${NSD_CreateLabel} 0 0 100% 40u "If you have a USB license key, please insert it into one of your computer's \
	    USB port and select 'Refresh'. The license file on the key should automatically be selected.  You may \
		also manually browse for the license file."   
	Pop $LDLabel


    ${NSD_CreateText} 0 50% 75% 12u "$LicenseFile"
    Pop $LDFileEdit
	GetFunctionAddress $0 OnFileEditChange
	nsDialogs::OnChange $LDFileEdit $0


	${NSD_CreateButton} 80% 50% 20% 12u "Browse..."
	Pop $LDBrowseButton
	GetFunctionAddress $0 OnBrowseButtonClick
	nsDialogs::OnClick $LDBrowseButton $0

	${NSD_CreateButton} 80% 60% 20% 12u "Refresh"
	Pop $LDRefreshButton
	GetFunctionAddress $0 OnRefreshButtonClick
	nsDialogs::OnClick $LDRefreshButton $0
  
    nsDialogs::Show
FunctionEnd

Function OnRefreshButtonClick
    InstallerHelper::DefaultLicenseFilePath
    Pop $1

   	SendMessage $LDFileEdit ${WM_SETTEXT} 0 "STR:$1" 

	StrCpy $LicenseFile $1
FunctionEnd

Function OnBrowseButtonClick
	Pop $0 # HWND

    nsDialogs::SelectFileDialog  open  ""  "Thermo Fisher Scientific License File|*.lic"
    Pop $2
    
	${If} $2 == ""
		; Do nothing
	${ElseIf} $2 != "error" 
	
		${NSD_SetText}  $LDFileEdit $2

    ${Else}
    ${EndIf}

FunctionEnd

Function OnFileEditChange
	Pop $0 # HWND

	${NSD_GetText} $LDFileEdit $LicenseFile

FunctionEnd

Function CheckLicenseStatus
	Pop $0

	${Switch} $0
		${Case} ${LSValid}
		${Break}

		${Case} ${LSExpiredClose}
		${Break}

		${Case} ${LSLicenseFileNotFound}
			; MessageBox MB_OK "No license key found."
		${Break}

		${Case} ${LSInvalidKey}
			MessageBox MB_OK "License file is not valid."
		${Break}

		${Case} ${LSInvalidSoftwareVersion}
			MessageBox MB_OK "Installer version newer than version allowed by key."
		${Break}

		${Case} ${LSInvalidSerialNumber}
			MessageBox MB_OK "Serial number does not match key."
		${Break}

		${Case} ${LSExpired}
			MessageBox MB_OK "License key has expired."
		${Break}

		${Default}
			MessageBox MB_OK "Check license invalid response $0"
		 ${Break}
	${EndSwitch}
FunctionEnd

Function LDLeave 
	InstallerHelper::CloseLicenseFile 
    InstallerHelper::OpenLicenseFile  $LicenseFile ${VERSION}
    Pop $LDStatus


	Push $LDStatus
	call CheckLicenseStatus


	${If} $LDStatus != ${LSValid} 
		${AndIf} $LDStatus != ${LSLicenseFileNotFound}
		Abort
	${Endif}

	InstallerHelper::FeatureEnabled "NIST-EPA"
	Pop $0

	${If} $0 == 0
	    StrCpy $NISTLibraryEnabled "false"
	${Else}
	    StrCpy $NISTLibraryEnabled "true"
	${EndIf}

	InstallerHelper::FeatureEnabled "MAX1-Acquisition"
	Pop $0

	${If} $0 == 0
	    StrCpy $Max1Enabled "false"
	${Else}
	    StrCpy $Max1Enabled "true"
	${EndIf}

	InstallerHelper::FeatureEnabled "MAX2-Acquisition"
	Pop $0

	${If} $0 == 0
	    StrCpy $Max2Enabled "false"
	${Else}
	    StrCpy $Max2Enabled "true"
	${EndIf}

	InstallerHelper::FeatureEnabled "MAX-Analytics"
	Pop $0

	${If} $0 == 0
	    StrCpy $MaxAnalyticsEnabled "false"
	${Else}
	    StrCpy $MaxAnalyticsEnabled "true"
	${EndIf}

	Call LicenseUpdateSections

FunctionEnd

Function InstallLocalKeyIfNeeded
	InstallerHelper::InstallLocalKeyIfNeeded $LicenseFile
FunctionEnd
