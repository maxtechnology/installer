!include WinVer.nsh
!define OSWarningMsg "is not supported by software.  Do you want to try to install anyway?"

Function OSVersionCheck 

    ${If} ${IsWinVista}
        MessageBox MB_YESNO|MB_ICONEXCLAMATION "Vista ${OSWarningMsg}"  IDNO QUIT

    ${ElseIf} ${IsWin8} 
        ${AndIfNot} ${AtLeastServicePack} 1
        MessageBox MB_YESNO|MB_ICONEXCLAMATION "Windows 8 before SP1 ${OSWarningMsg}"  IDNO QUIT

    ${ElseIf} ${IsWin7} 
        ${AndIfNot} ${AtLeastServicePack} 1
        MessageBox MB_YESNO|MB_ICONEXCLAMATION "Windows 7 before SP1 ${OSWarningMsg}"  IDNO QUIT

    ${ElseIf} ${IsWinME}
        MessageBox MB_YESNO|MB_ICONEXCLAMATION "Windows ME ${OSWarningMsg}"  IDNO QUIT
    
    ${ElseIf} ${IsWinXP}
        MessageBox MB_YESNO|MB_ICONEXCLAMATION "XP ${OSWarningMsg}" IDNO QUIT
       
    ${ElseIf} ${IsWin95}
        MessageBox MB_YESNO|MB_ICONEXCLAMATION "Windows 95  ${OSWarningMsg}" IDNO QUIT
    
    ${ElseIf} ${IsWin98}
        MessageBox MB_YESNO|MB_ICONEXCLAMATION "Windows 98 ${OSWarningMsg}" IDNO QUIT

    ${ElseIf} ${IsWin2003}
        MessageBox MB_YESNO|MB_ICONEXCLAMATION "Windows Server 2003 ${OSWarningMsg}" IDNO QUIT

    ${ElseIf} ${IsWin2008}
        MessageBox MB_YESNO|MB_ICONEXCLAMATION "Windows Server 2008 ${OSWarningMsg}" IDNO QUIT

    ${ElseIf} ${IsWin2008R2}
        MessageBox MB_YESNO|MB_ICONEXCLAMATION "Windows Server 2008 SP2 ${OSWarningMsg}" IDNO QUIT

    ${EndIf}


    Return

    QUIT:
        Abort

FunctionEnd