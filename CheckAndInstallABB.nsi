;
;  Installation of the drivers needed by the ABB spectrometer
;
!define ABBLOGNAME "${LOG_DIR}\abbinstallLog.txt"

Function CheckAndInstallABB
    SetOutPath "$TEMP"
    Delete "${ABBLOGNAME}"
    File /r "3rd party installers\ABB" 

  	ClearErrors
    ExecWait  '"msiexec" /i "$TEMP\ABB\OEM-MB3000AcquisitionServer.msi" /passive /log "${ABBLOGNAME}"'
    
    IfErrors error_installing_abb

    goto done

    error_installing_abb:
    MessageBox MB_OK 'Error installing ABB driver. See log file ${ABBLOGNAME}'

	done:
    RMDir /r "$TEMP\ABB"
FunctionEnd