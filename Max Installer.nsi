;MAX Installer

;--------------------------------
; Global defines
  !define APP_NAME "MAX Analytical"

  ; Note that the below define is provided to the installer on the command line
  ; !define INCLUDE_3RD_PARTY

;--------------------------------

;--------------------------------
; Include Section
  !include "GlobalDefines.nsi"
  !include "GlobalVars.nsi"
  !include "InstallMSXML.nsi"
  !include "OSVersionCheck.nsi"

  ; Modern UI
  !include "MUI2.nsh"

  !ifdef INCLUDE_3RD_PARTY
    !include "CheckAndInstallDotNet.nsi"
    !include "CheckAndInstallABB.nsi"
    !include "CheckAndInstallBBSmartWorx.nsi"
  !endif

  !include "FilePromptPage.nsi"
  !include "DefaultDataDir.nsi"
  ;!include "CreateDataFolder.nsi"
  !include "RegisterPGRF.nsi"
  !include "RegisterRIC.nsi"
  !include "RegisterRIDB.nsi"
  !include "RegisterQTA.nsi"

;--------------------------------

;--------------------------------
; General Settings

  ;Name and file
  Name "${FULL_NAME}"
  !ifdef INCLUDE_3RD_PARTY
    OutFile "${FULL_NAME} Installer.exe"
  !else
    OutFile "${FULL_NAME} Updater.exe"
  !endif

  ;Default installation folder
  InstallDir "$PROGRAMFILES64\${APP_NAME}"

  ;Get installation folder from registry if available
  ; InstallDirRegKey HKLM  "Software\${FULL_NAME}" ""

  ;Request application privileges (this is now handled by the multiuser init)
  ; RequestExecutionLevel admin

  ;Initialize the installer enviroment for a multiuser installation
  !define MULTIUSER_EXECUTIONLEVEL Admin
  !include MultiUser.nsh
;--------------------------------

;--------------------------------
; Modern UI Interface Settings

  !define MUI_ABORTWARNING
  
  !define MUI_ICON "images\installer.ico"
  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP "images\installer.bmp"
  !define MUI_HEADERIMAGE_RIGHT
;--------------------------------
  
;--------------------------------
; Customize the finish page for our needs. 
  !define MUI_FINISHPAGE_SHOWREADME ""
  !define MUI_FINISHPAGE_SHOWREADME_CHECKED
  !define MUI_FINISHPAGE_SHOWREADME_TEXT "Create Desktop Shortcut"
  !define MUI_FINISHPAGE_SHOWREADME_FUNCTION InstallDeskTopShortCutsCB
  
  !define MUI_FINISHPAGE_LINK "Thermo Fisher Scientific"
  !define MUI_FINISHPAGE_LINK_LOCATION "https://www.thermofisher.com/"
;--------------------------------

;--------------------------------
; Pages in the installer

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "License.rtf"

  Page custom LicensePromptPage LDLeave
  
  !insertmacro MUI_PAGE_COMPONENTS  
  !insertmacro MUI_PAGE_DIRECTORY

  
  !define MUI_DIRECTORYPAGE_TEXT_TOP "Setup will install ${FULL_NAME} in the following folder. You \
  should selected a folder on a drive with a lot of free space as this folder will be used to store \
  large spectrum data sets.  To install in a different folder, click Browse and selectg another \
  folder. Click install to start the installation" 
  !define MUI_DIRECTORYPAGE_TEXT_DESTINATION "Data and Quant Library Folder"
  !define MUI_DIRECTORYPAGE_VARIABLE $DataDir
  !insertmacro MUI_PAGE_DIRECTORY
  
  !insertmacro MUI_PAGE_INSTFILES
  
  ; Page custom PostInstallFunction
	
  !insertmacro MUI_PAGE_FINISH

; Uninstall screens
  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH

;--------------------------------
; Languages
  !insertmacro MUI_LANGUAGE "English"
;--------------------------------

;--------------------------------
; Analytic Software Installer Sections
Section "Analytic Software" SecAnalytic  

  Call RegisterPGRF	
  Call RegisterRIC	
  Call RegisterRIDB	
  Call RegisterQTA
				 
  StrCpy $InstallAnalytics "true"
  
SectionEnd

;--------------------------------
; QTA Viewer Installer Sections
Section "QTA Viewer Software" SecQTAViewer  

  Call RegisterQTA
				 
  StrCpy $InstallQTAViewer "true"
  
SectionEnd
 


;--------------------------------
; Max 2 Acquisition Software Installer Sections 
Section "MAX Acquisition Software" SecMAX2Acquisition
  !ifdef INCLUDE_3RD_PARTY

    Call CheckAndInstallABB	
    call CheckAndInstallBBSmartWorx
  

  !endif

  StrCpy $InstallMax2 "true"
SectionEnd
  
;--------------------------------
; Section Descriptions

  ; Language strings
  LangString DESC_SecAnalytic ${LANG_ENGLISH} "Software used to analyze spectrum data after it is collected."
  LangString DESC_SecQTAViewer ${LANG_ENGLISH} "Software used to view QTA files."
  LangString DESC_SecMAX2Acquisition ${LANG_ENGLISH} "Software used to acquire spectrum data from MAX Spectrometer."

  

  ; Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecAnalytic} $(DESC_SecAnalytic)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecQTAViewer} $(DESC_SecQTAViewer)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecMAX2Acquisition} $(DESC_SecMAX2Acquisition)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END
;-------------------------------- 

;--------------------------------
; Uninstaller Section
Section "Uninstall"
  SetRegView 64

  RMDir /r "$INSTDIR\bin"
  RMDir /r "$INSTDIR\Documentation"
  RMDir /r "$INSTDIR\3rd party installers"

  RMDir /r "$SMPROGRAMS\${APP_NAME}"
  
  
  Delete "$INSTDIR\Uninstall.exe"

  RMDir "$INSTDIR"

  DeleteRegKey /ifempty HKCU "Software\${APP_NAME}"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}"
			  
  call Un.RegisterPGRF
  call Un.RegisterRIC
  call Un.RegisterRIDB
  call Un.RegisterQTA 
  
  ;Remove desktop icons
    Delete  "$DESKTOP\MAX-Analytics.lnk" 
    Delete  "$DESKTOP\MAX CL Calibration Editor.lnk"    
    Delete  "$DESKTOP\MAX Gas Reference Editor.lnk"  
    Delete  "$DESKTOP\MAX RIDB Editor.lnk"             
	  Delete  "$DESKTOP\MAX QTA Viewer.lnk"  
    Delete  "$DESKTOP\MAX 1 Acquisition.lnk"   
    Delete  "$DESKTOP\MAX Acquisition.lnk"     
SectionEnd

;--------------------------------
; Stuff to do after NSIS initialzes 
; Sets installer up to install 64 bit, finds default data directory
Function .onInit
  Call OSVersionCheck

  Call InitializeGlobals
  Call InstallMSXML

  SetRegView 64
  !insertmacro MULTIUSER_INIT 
  Call DefaultDataDir  

FunctionEnd 

;--------------------------------
; Called when the installer hits the finish page.  Where a lot of the real work is 
; done.
Function .onInstSuccess
  !ifdef INCLUDE_3RD_PARTY

    Call CheckAndInstallDotNet

  !endif

  ${If} $InstallAnalytics == "true" 
  ${OrIf} $InstallMax2 == "true"
  ${OrIf} $InstallQTAViewer == "true"

    SetOutPath "$INSTDIR\bin"
    File /r files\analysis\* 
  ${EndIf}

  SetOutPath "$INSTDIR\Documentation"
  File /r files\Documentation\* 
  File License.rtf

  Call InstallLocalKeyIfNeeded
	
	; Create menu commands common to all installations
  CreateDirectory "$SMPROGRAMS\${APP_NAME}\Documentation"
	CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX PGRF Editor Guide.lnk"      "$INSTDIR\Documentation\MAX PGRF Editor.pdf"
  CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX Quick Start Guide.lnk"      "$INSTDIR\Documentation\MAX Quick Start Guide.pdf"
	
	CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX Excel Template Reporting.lnk"      "$INSTDIR\Documentation\MAX Excel Template Reporting.pdf"
  CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX Manual Validation Mode Manual.lnk" "$INSTDIR\Documentation\MAX Manual Validation Mode Manual.pdf"


  ; Name the renaming of Reference Library to NIST & EPA Library
	; This should be removed after we're sure every one inhouse updated to this version or higher
	/*
  IfFileExists "$DataDir\Reference Library\*.*" REF_EXIST REF_CHECK_END
	REF_EXIST:
	IfFileExists "$DataDir\NIST & EPA Library\*.*" REF_CHECK_END NIST_NOT_EXIST
	NIST_NOT_EXIST:
	rename "$DataDir\Reference Library\" "$DataDir\NIST & EPA Library\"
	REF_CHECK_END:
  */

	; Call CreateDataFolder
	
  WriteRegStr HKLM "Software\${DATA_KEY_NAME}" "DataDir" "$DataDir"

	SetOutPath "$DataDir\data"
  File /r files\data\*
  File License.rtf 
	; File License.rtf files\data\*
	
  ; Moving into InstallationConfiguration
  ; SetOutPath "$DataDir\InstrumentMethods"
  ; File /r "files\Instrument Methods\*" 
	

	; CreateShortcut  "$SMPROGRAMS\${APP_NAME}\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
	
	; Write the estimate installation size, so add/remove program can show the right values
	${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
  IntFmt $0 "0x%08X" $0
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
		"EstimatedSize" "$0"

  ;Store installation folder
  WriteRegStr HKLM  "Software\${APP_NAME}" "" "$\"$INSTDIR$\""
  
  	;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

  ;Add Remove registry keys
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
                 "DisplayName" "${FULL_NAME}" 
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
                 "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
				 "Publisher" "${COMPANY}"

 

  ${If} $InstallAnalytics == "true" 

    ${If} $InstallDeskTopShortCuts == "true" 

      ; Create Desktop links for Max Analytic

      CreateShortcut  "$DESKTOP\MAX-Analytics.lnk"             "$INSTDIR\bin\MAX-Analytics.exe"
      CreateShortcut  "$DESKTOP\MAX Gas Reference Editor.lnk"  "$INSTDIR\bin\GasReferenceFileEditor.exe"
	    CreateShortcut  "$DESKTOP\MAX QTA Viewer.lnk"            "$INSTDIR\bin\QTAViewer.exe"

      ; CreateShortcut  "$DESKTOP\MAX CL Calibration Editor.lnk"  "$INSTDIR\bin\CLCalibrationEditor.exe"
      ; CreateShortcut  "$DESKTOP\MAX RIDB Editor.lnk"           "$INSTDIR\bin\RIDBEditor.exe"


    ${EndIf}

    ; Create start menu short cuts	
    CreateDirectory "$SMPROGRAMS\${APP_NAME}"

    CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX-Analytics.lnk"            "$INSTDIR\bin\MAX-Analytics.exe"
    CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX Gas Reference Editor.lnk" "$INSTDIR\bin\GasReferenceFileEditor.exe"
    CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX QTA Viewer.lnk"           "$INSTDIR\bin\QTAViewer.exe"
    CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX Clear Preferences.lnk"    "$INSTDIR\bin\ClearMaxPreferences.exe"

    ; CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX Virtual Spiker.lnk"         "$INSTDIR\bin\Virtual Spiker.exe"
    ; CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX RIDB Editor.lnk"             "$INSTDIR\bin\RIDBEditor.exe"
    ; CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX CL Calibration Editor.lnk"    "$INSTDIR\bin\CLCalibrationEditor.exe"
  ${EndIf}

   
  ${If} $InstallQTAViewer == "true" 

    ${If} $InstallDeskTopShortCuts == "true" 

      ; Create Desktop links for Max Analytic

	    CreateShortcut  "$DESKTOP\MAX QTA Viewer.lnk"            "$INSTDIR\bin\QTAViewer.exe"

    ${EndIf}

    ; Create start menu short cuts	
    CreateDirectory "$SMPROGRAMS\${APP_NAME}"
    CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX QTA Viewer.lnk"              "$INSTDIR\bin\QTAViewer.exe"

  ${EndIf}

   

  ${If} $InstallMax2 == "true" 
    ;Create Desktop links for Max 2 Acquisition
    
    ${If} $InstallDeskTopShortCuts == "true" 
      CreateShortcut "$DESKTOP\MAX Acquisition.lnk"            "$INSTDIR\bin\MAX 2 Acquisition.exe"
      CreateShortcut  "$DESKTOP\MAX QTA Viewer.lnk"            "$INSTDIR\bin\QTAViewer.exe"
      CreateShortcut  "$DESKTOP\MAX Gas Reference Editor.lnk"  "$INSTDIR\bin\GasReferenceFileEditor.exe"

      ; CreateShortcut  "$DESKTOP\MAX CL Calibration Editor.lnk"  "$INSTDIR\bin\CLCalibrationEditor.exe"
      ; CreateShortcut  "$DESKTOP\MAX RIDB Editor.lnk"           "$INSTDIR\bin\RIDBEditor.exe"
    ${EndIf}

    ; Create start menu short cuts	
    CreateDirectory "$SMPROGRAMS\${APP_NAME}"
    CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX Acquisition.lnk"              "$INSTDIR\bin\MAX 2 Acquisition.exe"
    CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX QTA Viewer.lnk"               "$INSTDIR\bin\QTAViewer.exe"
    CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX CL Calibration Editor.lnk"    "$INSTDIR\bin\CLCalibrationEditor.exe"
    CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX RIDB Editor.lnk"              "$INSTDIR\bin\RIDBEditor.exe"
    CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX Clear Preferences.lnk"        "$INSTDIR\bin\ClearMaxPreferences.exe"
    CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX Gas Reference Editor.lnk"     "$INSTDIR\bin\GasReferenceFileEditor.exe"
    CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MAX Modbus Monitor.lnk"           "$INSTDIR\bin\Modbus Monitor.exe"

    CreateShortcut  "$SMPROGRAMS\${APP_NAME}\MultiGas 2030 with StarBoost Quick Start Guide.lnk"      "$INSTDIR\Documentation\MultiGas 2030 with StarBoost Quick Start Guide.pdf"
  ${EndIf}

  ${If} ${FileExists} "$INSTDIR\bin\InstallerConfiguration.exe"
    ExecWait '$INSTDIR\bin\InstallerConfiguration.exe'
  ${EndIf}

 
  ; Call that forces the desktop (all windows) to refresh
  ; Kind of needed to make deleted short cuts go away immediately after install
  ; otherwise manual refresh of desktop is needed
  System::Call 'Shell32::SHChangeNotify(i 0x8000000, i 0, i 0, i 0)'


FunctionEnd

;--------------------------------
; Handles hiding sections that are not licensed
Function LicenseUpdateSections
  ${If} $Max2Enabled == "false" 
    SectionSetFlags ${SecMAX2Acquisition} ${SF_RO}
    SectionSetText  ${SecMAX2Acquisition} ""
  ${else}
  ;  IntOp $0 ${SF_SELECTED} | ${SF_SELECTED}
    SectionSetFlags ${SecMAX2Acquisition} ${SF_SELECTED}
    SectionSetText  ${SecMAX2Acquisition}  "MAX Acquisition Software" 
  ${EndIf}

  ${If} $MaxAnalyticsEnabled == "false" 
    SectionSetFlags ${SecAnalytic} ${SF_RO}
    SectionSetText  ${SecAnalytic} ""
  ${else}
  ;  IntOp $0 ${SF_SELECTED} | ${SF_SELECTED}
    SectionSetFlags ${SecAnalytic} ${SF_SELECTED}
    SectionSetText  ${SecAnalytic}  "Analytic Software" 
  ${EndIf}

  ${If} $QTAViewerEnabled == "false" 
    SectionSetFlags ${SecQTAViewer} ${SF_RO}
    SectionSetText  ${SecQTAViewer} ""
  ${else}
  ;  IntOp $0 ${SF_SELECTED} | ${SF_SELECTED}
    SectionSetFlags ${SecQTAViewer} ${SF_SELECTED}
    SectionSetText  ${SecQTAViewer}  "QTA Viewer Software" 
  ${EndIf}




FunctionEnd


;--------------------------------
; Stuff to do on unintall
Function un.onInit
  !insertmacro MULTIUSER_UNINIT
FunctionEnd

;--------------------------------
; Change finish page prompt for short cuts
Function InstallDeskTopShortCutsCB
  StrCpy $InstallDeskTopShortCuts "true"

FunctionEnd
