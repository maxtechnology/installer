Function RegisterQTA  

WriteRegStr HKCR ".qta" "" "QTAFile"
WriteRegStr HKCR "QTAFile" "" "Thermo Fisher Scientific Quant Analytic File"
WriteRegStr HKCR "QTAFile\DefaultIcon" "" "$INSTDIR\bin\images\QTAFile.ico"

WriteRegStr HKCR "QTAFile\shell\open" "" "&Open"
WriteRegStr HKCR "QTAFile\shell\open\command" "" '$INSTDIR\bin\QTAViewer.exe  /open "%1"'

FunctionEnd


Function Un.RegisterQTA  

  DeleteRegKey HKCR ".qta"
  DeleteRegKey HKCR QTAFile
FunctionEnd