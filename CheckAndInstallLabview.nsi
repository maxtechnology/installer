!define LVLOGNAME "${LOG_DIR}\LabVIEW 2015 SP1.log"

; No longer Used
Function CheckAndInstallLabView
    SetOutPath "$TEMP"
    File /r "3rd party installers\LabView" 

	Delete "${LVLOGNAME}"
    ClearErrors
    ; ReadRegStr  $0 HKLM "SOFTWARE\WOW6432Node\National Instruments\LabVIEW Run-Time\15.0" "Version"


	
    ;IfErrors NotDetected


    ;    DetailPrint "National Instruments Labview 2015 SP1 is installed"
	;	goto done


    ; NotDetected:
		
		
        DetailPrint "Installing National Instruments Labview 2015 SP1"
        SetDetailsPrint listonly
        ExecWait '"$TEMP\LabView\setup.exe" /qb /AcceptLicenses yes /r:n /log "${LVLOGNAME}"' $0
			
		IfErrors error_installing_labview
		
        SetDetailsPrint lastused
        DetailPrint ""

	goto done

	error_installing_labview:
	MessageBox MB_OK 'Error installing National Instruments Labview 2015 SP1. See log file ${LVLOGNAME}'
	
	done:
	  
    RMDir /r "$TEMP\LabView"  
FunctionEnd