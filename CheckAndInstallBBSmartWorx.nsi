;
; Installs RS-485 driver used by the MAX-IR temp/pressure sensor
;
!include x64.nsh
!define BBSLOGNAME "${LOG_DIR}\BBSmartWorxinstallLog.txt"

Function CheckAndInstallBBSmartWorx
    SetOutPath "$TEMP"
    Delete "${BBSLOGNAME}"
    File /r "3rd party installers\BBSmartWorx" 

  	ClearErrors

    ${DisableX64FSRedirection}
    nsExec::ExecToStack '"$SYSDIR\PnPutil.exe" /add-driver "$TEMP\BBSmartWorx\B_B USB485TB-2W\ftdibus.inf"'
    ${EnableX64FSRedirection}
   
    Pop $0 # return value/error/timeout
    Pop $1 # printed text, up to ${NSIS_MAX_STRLEN}

	  ${If} $0 <> 0
      goto error_installing_BB
    ${EndIf}

    ${DisableX64FSRedirection}
    nsExec::ExecToStack '"$SYSDIR\PnPutil.exe" /add-driver "$TEMP\BBSmartWorx\B_B USB485TB-2W\ftdiport.inf"'
    ${EnableX64FSRedirection}
   
    Pop $0 # return value/error/timeout
    Pop $1 # printed text, up to ${NSIS_MAX_STRLEN}

	  ${If} $0 <> 0
      goto error_installing_BB
    ${EndIf}

    ; ExecWait '"$TEMP\BBSmartWorx\dpinst64.exe"'
    
    ; No logging option
    ; /log "${BBSLOGNAME}"

    ; If no device is immedately found, installer returns error code 
    ; even though the driver gets installed. 
    ; IfErrors error_installing_BB

    ; MessageBox MB_OK 'PnP: $0 - $1'
    ; goto done


    goto done
    
    error_installing_BB:
    MessageBox MB_OK 'Error installing BBSmartWorx driver:  $1'

    ; No Logging opiton
    ; See log file ${BBSLOGNAME}

	done:
    RMDir /r "$TEMP\BBSmartWorx"
FunctionEnd