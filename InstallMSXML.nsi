
!define MSXMLLOGNAME "${LOG_DIR}\msxmlinstallLog.txt"
;--------------------------------
; Used by the installer's licensing 
; plugin 
Function InstallMSXML
    SetOutPath "$TEMP"
     Delete "${MSXMLLOGNAME}"
    File /r "3rd party installers\MSXML" 

  	ClearErrors
    ExecWait  '"msiexec" /i "$TEMP\MSXML\msxml6.msi" /passive /log "${MSXMLLOGNAME}"'
    
    IfErrors error_installing_msxml

    RMDir /r "$TEMP\MSXML"

    goto done

    error_installing_msxml:
    MessageBox MB_OK 'Error installing MSXML. See log file ${MSXMLLOGNAME}'
	
	done:
FunctionEnd
			