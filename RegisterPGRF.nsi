Function RegisterPGRF  

WriteRegStr HKCR ".pgrf" "" "PGRFile"
WriteRegStr HKCR "PGRFile" "" "Thermo Fisher Scientific Gas Reference File"
WriteRegStr HKCR "PGRFile\DefaultIcon" "" "$INSTDIR\bin\images\GasReferenceFile.ico"

WriteRegStr HKCR "PGRFile\shell\open" "" "&Open"
WriteRegStr HKCR "PGRFile\shell\open\command" "" '$INSTDIR\bin\GasReferenceFileEditor.exe  /open "%1"'

FunctionEnd


Function Un.RegisterPGRF  

  DeleteRegKey HKCR ".pgrf"
  DeleteRegKey HKCR PGRFile
FunctionEnd