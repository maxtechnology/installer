; !define DNLOGNAME "${LOG_DIR}\net4.8installLog.htm"
!define DNLOGNAME "${LOG_DIR}\core3.1installLog.htm"

Function CheckAndInstallDotNet
    SetOutPath "$TEMP"
    File /r "3rd party installers\DotNet" 

	Delete "${DNLOGNAME}"
    ClearErrors
    ; ReadRegDWORD $0 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full" "Release"

    ; IfErrors NotDetected

    ;${If} $0 >= 394254

    ;    DetailPrint "Microsoft .NET Framework 4.6.1 is installed"

    ;${Else}
    ; NotDetected:
			
        DetailPrint "Installing Microsoft Core 3.1"
        SetDetailsPrint listonly
        ExecWait '"$TEMP\DotNet\windowsdesktop-runtime-5.0.6-win-x64.exe" /norestart /passive /showrmui /log "${DNLOGNAME}"' $0
        ; ExecWait '"$TEMP\DotNet\windowsdesktop-runtime-3.1.0-win-x64.exe" /norestart /passive /showrmui /log "${DNLOGNAME}"' $0
        ; ExecWait '"$TEMP\DotNet\ndp48-web.exe" /norestart /passive /showrmui /log "${DNLOGNAME}"' $0
			
		IfErrors error_installing_dotnet
		
        SetDetailsPrint lastused
        DetailPrint ""
    ; ${EndIf}
	goto done

	error_installing_dotnet:
	MessageBox MB_OK 'Error installing Core 3.1. See log file ${DNLOGNAME}'
	
	done:
	RMDir /r "$TEMP\DotNet"
FunctionEnd