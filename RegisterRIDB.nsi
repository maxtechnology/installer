Function RegisterRIDB  

WriteRegStr HKCR ".ridb" "" "RIDBFile"
WriteRegStr HKCR "RIDBFile" "" "Thermo Fisher Scientific Retention Index Database"
WriteRegStr HKCR "RIDBFile\DefaultIcon" "" "$INSTDIR\bin\images\RIDBFile.ico"

WriteRegStr HKCR "RIDBFile\shell\open" "" "&Open"
WriteRegStr HKCR "RIDBFile\shell\open\command" "" '$INSTDIR\bin\RIDBEditor.exe  /open "%1"'

FunctionEnd


Function Un.RegisterRIDB  

  DeleteRegKey HKCR ".ridb"
  DeleteRegKey HKCR RIDBFile
FunctionEnd