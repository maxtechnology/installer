!define LOCALUSERS "(S-1-5-32-545)"
!define EVERYONESID "(S-1-1-0)"
!define AUTHENICATEDSID "(S-1-5-11)"

Function CreateDataFolder  
    
	ClearErrors
	CreateDirectory "$DataDir\Quant Library"
	${If} ${Errors}
	
	    MessageBox MB_OK|MB_ICONSTOP  "Failed to create data folder $DataDir\Quant Library"
	
	${EndIf}
  
  	ClearErrors
	CreateDirectory "$DataDir\NIST & EPA Library"
	${If} ${Errors}
	
	    MessageBox MB_OK|MB_ICONSTOP  "Failed to create data folder $DataDir\NIST & EPA Library"
	
	${EndIf}
	
    ClearErrors
	CreateDirectory "$DataDir\Samples"
	${If} ${Errors}
	
	    MessageBox MB_OK|MB_ICONSTOP  "Failed to create data folder $DataDir\Samples"
	
	${EndIf}
  
  	ClearErrors
    AccessControl::GrantOnFile "$DataDir" "${AUTHENICATEDSID}" "FullAccess"
	
	${If} ${Errors}
	
	    MessageBox MB_OK|MB_ICONSTOP  "Failed to set permissions on $DataDir"
	
	${EndIf}
	
  
  	ClearErrors
    ; Store where the data directory is
    WriteRegStr HKLM "Software\${DATA_KEY_NAME}" "DataDir" "$DataDir"
	
	
	${If} ${Errors}
	
	    MessageBox MB_OK|MB_ICONSTOP  "Failed write registry entry for data directory location."
	
	${EndIf}
				 
				 
	ClearErrors
    AccessControl::GrantOnRegKey \
      HKLM "Software\${DATA_KEY_NAME}" "${AUTHENICATEDSID}" "FullAccess"
	  
	  
	 ${If} ${Errors}
	
	    MessageBox MB_OK|MB_ICONSTOP  "Failed to set permissions on data directory registry key."
	
	${EndIf}
				 
FunctionEnd