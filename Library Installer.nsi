;Quant Library Installer

;--------------------------------
; Global defines
  !define APP_NAME "MAX Analytical Quant Library"
;--------------------------------

;--------------------------------
; Include Section
  !include "GlobalDefines.nsi"
  !include "GlobalVars.nsi"
  !include "InstallMSXML.nsi"

  ; Modern UI
  !include "MUI2.nsh"

  !include "DefaultDataDir.nsi"
  !include "FilePromptPage.nsi"

  ; !include "CreateDataFolder.nsi"
;--------------------------------

;--------------------------------
;General

  ;Name and file
  
  Name "${FULL_NAME}"
  OutFile "${FULL_NAME} Installer.exe"

  ;Default installation folder
  InstallDir "$PROGRAMFILES64\${APP_NAME}"


  ;Request application privileges (this is now handled by the multiuser init)
  ; RequestExecutionLevel admin

  ;Initialize the installer enviroment for a multiuser installation
  !define MULTIUSER_EXECUTIONLEVEL Admin
  !include MultiUser.nsh
;--------------------------------

;--------------------------------
; Modern UI Interface Settings

  !define MUI_ABORTWARNING
  
  !define MUI_ICON "images\installer.ico"
  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP "images\installer.bmp"
  !define MUI_HEADERIMAGE_RIGHT
;--------------------------------
  
;--------------------------------
; Change finish page prompt for short cuts
 
  !define MUI_FINISHPAGE_LINK "Thermo Fisher Scientific"
  !define MUI_FINISHPAGE_LINK_LOCATION "https://www.thermofisher.com/"
;--------------------------------

;--------------------------------
; Pages in the installer

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "License.rtf"
  
  Page custom LicensePromptPage LDLeave

  !insertmacro MUI_PAGE_COMPONENTS

  !define MUI_DIRECTORYPAGE_TEXT_TOP "Setup will install ${FULL_NAME} in the following folder. You \
  should selected a folder on a drive with a lot of free space as this folder will also be used to store \
  large spectrum data sets.  To install in a different folder, click Browse and selectg another \
  folder. Click install to start the installation" 
  !define MUI_DIRECTORYPAGE_TEXT_DESTINATION "Data and Quant Library Folder"
  !define MUI_DIRECTORYPAGE_VARIABLE $DataDir
  !insertmacro MUI_PAGE_DIRECTORY
  


  !insertmacro MUI_PAGE_INSTFILES
  
  Page custom PostInstallFunction
	
  !insertmacro MUI_PAGE_FINISH

  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH
;--------------------------------

;--------------------------------
; Languages
  !insertmacro MUI_LANGUAGE "English"
;------------------------------
 
;--------------------------------
; Quant Library Installer Sections
Section "Quant Library" SecQuantLibrary

  ; Call CreateDataFolder
  
  SetOutPath "$DataDir\Quant Library"
  File /r "files\Quant Library\*" 
  
SectionEnd
  
;--------------------------------
; NIST & EPA Installer Sections
Section "NIST & EPA Library" SecReferenceLibrary

  ; Call CreateDataFolder
  
  SetOutPath "$DataDir\NIST & EPA Library"
  File /r "files\NIST & EPA Library\*" 
  
SectionEnd

;--------------------------------
; Samples Sections
Section "Samples" SecSampleDatasets

  ; Call CreateDataFolder
  
	  SetOutPath "$DataDir\Samples"
	  File /r files\Samples\*
  
SectionEnd
  
;--------------------------------
; Section Descriptions

  ;Language strings
  LangString DESC_SecQuantLibrary ${LANG_ENGLISH} "Library of gas reference files used for quanting."
  LangString DESC_SecReferenceLibrary ${LANG_ENGLISH} "NIST & EPA Library of gas reference files used for identifying unknowns."
  LangString DESC_SecSampleDatasets ${LANG_ENGLISH} "Example Datasets."

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	  !insertmacro MUI_DESCRIPTION_TEXT ${SecQuantLibrary} $(DESC_SecQuantLibrary)
	  !insertmacro MUI_DESCRIPTION_TEXT ${SecReferenceLibrary} $(DESC_SecReferenceLibrary)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecSampleDatasets} $(DESC_SecSampleDatasets)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END
;-------------------------------- 

  
;--------------------------------
;Uninstaller Section
Section "Uninstall"

  Delete "$INSTDIR\Uninstall.exe"

  RMDir "$INSTDIR"

  DeleteRegKey /ifempty HKCU "Software\${APP_NAME}"

SectionEnd


;--------------------------------
; Stuff to do after NSIS initialzes 
; Sets installer up to install 64 bit, finds default data directory
Function .onInit
  Call InitializeGlobals
  Call InstallMSXML
  
  SetRegView 64
  !insertmacro MULTIUSER_INIT
  
  Call DefaultDataDir	 
	  

FunctionEnd 

Function LicenseUpdateSections
  ${If} $NISTLibraryEnabled == "false" 
    SectionSetFlags ${SecReferenceLibrary} ${SF_RO}
    SectionSetText  ${SecReferenceLibrary} ""
  ${else}
    SectionSetFlags ${SecReferenceLibrary} ${SF_SELECTED}
    SectionSetText  ${SecReferenceLibrary} "NIST & EPA Library"
  ${EndIf}
FunctionEnd

;--------------------------------
; Stuff to do on unintall
Function un.onInit
  !insertmacro MULTIUSER_UNINIT
  
FunctionEnd
  
;--------------------------------
; Stuff to do post installation
Function PostInstallFunction

  Call InstallLocalKeyIfNeeded

	; Write the estimate installation size, so add/remove program can show the right values
	${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
    IntFmt $0 "0x%08X" $0
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
		"EstimatedSize" "$0"
 
  WriteRegStr HKLM "Software\${DATA_KEY_NAME}" "DataDir" "$DataDir"
FunctionEnd
