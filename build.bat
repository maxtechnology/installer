@echo off

rem Installers to build
set BuildMAXUpdater=true
set BuildLibrary=true
set BuildMAX=true

rem Do we need to sign the installers
set SignInstaller=true

rem Value is provided to NSIS via command line.  Set in  GlobalDefines.nsi
rem Don't double quote that string below. 
rem set buildversion=3.7 Beta
set buildversion=3.14 Beta

rem Top level NSI files for the builds.
rem Note that the updater and main installer for MAX Analytical 
rem share a common MSI build root.  A NSI define statement is used
rem to differetiate what gets included. 
set nisfile="Max Installer.nsi"
set nisqlfile="Library Installer.nsi"

rem Directory to put the final builds in
set buildfolder="Build"

rem Though called; its output is not really used.  Builds a manifest for
rem the online update process.  We just don't use it.  This exe is part of
rem of the MAX anaysis build. 
set aubuilder=files\analysis\AutoUpdateManifestBuilder.exe

set analysisdir=files\analysis
set analysiszip="files\analysis.zip"
set sampleszip="files\Samples.zip"

set acquisitiondir=files\acquisition
set acquisitionzip="files\Max Aquisition.zip"
set datadir="files\data"
set samplesdir="files\Samples"


set quantlibdir="files\Quant Library\"
REM set quantlibzip="files\Quant Library.zip"

set zipcom="C:\Program Files\7-Zip\7z.exe"
set zipcomextract=%zipcom% -y x  
set nsiscom="C:\Program Files (x86)\NSIS\makensis.exe"

set WriteError="powershell" write-host -back red -fore black
set WriteSuccess="powershell" write-host -back green -fore black


REM Check that data files and executables needed exist in this environment
REM We current are not checking for the needed files in '3rd party installers'.  
REM None of these files are under source control 
IF NOT exist %zipcom% (
	%WriteError% Missing zip build tool: '%zipcom%' 
	GOTO Error
)


IF NOT exist %nsiscom% (
	%WriteError% Missing NSIS build tool: '%nsiscom%'
	GOTO Error
)


IF NOT exist %analysiszip% (
	%WriteError% Missing data file: '%analysiszip%'
	GOTO Error
)

IF NOT exist %quantlibdir% (
	%WriteError% Missing quant library file: '%quantlibdir%'
	GOTO Error
)

IF NOT exist %sampleszip% (
	%WriteError% Missing data file: '%sampleszip%' 
	GOTO Error
)

IF NOT exist "tools\signtool.exe" (
	%WriteError% Missing data file: "tools\signtool" 
	GOTO Error
)

IF %SignInstaller% == false (
	set signtool=echo
) else (
	rem set signtool="tools\signtool" sign  /debug /v /fd SHA256 /a /f tools\2019MaxCodeSigningKey.pfx /p e29a6cd189 
    rem set signtool="tools\signtool" sign  /debug /v /fd SHA256 /a /f tools\2020MaxCodeSigningKey.pfx /p e29a6cd189 
    set signtool="tools\signtool" sign  /debug /v /fd SHA256 /a /f tools\2021MaxCodeSigningKey.pfx /p 8a1e5b0dcb 
)


for /f %%x in ('wmic path win32_localtime get /format:list ^| findstr "="') do set %%x
set today=%Year%-%Month%-%Day%

set MaxInstaller="%buildfolder%\MAX Analytical %buildversion% Installer.exe"
set FinalMaxInstaller="%buildfolder%\MAX Analytical %buildversion% Installer %today%.exe"

set MaxUpdater="%buildfolder%\MAX Analytical %buildversion% Updater.exe"
set FinalMaxUpdater="%buildfolder%\MAX Analytical %buildversion% Updater %today%.exe"

set LibraryInstaller="%buildfolder%\MAX Analytical Quant Library %buildversion% Installer.exe"
set FinalLibraryInstaller="%buildfolder%\MAX Analytical Quant Library %buildversion% Installer %today%.exe"


rem Clear out old files folders before unzipping the "builds" into them.
rmdir /Q /S  %analysisdir% 
rmdir /Q /S  %acquisitiondir% 
rmdir /Q /S  %datadir% 
rmdir /Q /S  %samplesdir% 
rem rmdir /Q /S  %quantlibdir% 

rem Extract the builds into the folders that the installer build will be using
%zipcomextract% %analysiszip% -o%analysisdir%
if ERRORLEVEL 1 GOTO Error
%zipcomextract% %acquisitionzip% -o%acquisitiondir%
if ERRORLEVEL 1 GOTO Error
rem %zipcomextract% %quantlibzip% -o%quantlibdir%


rem Copy the data folder from the analysis builds into the top level data folder.  Note that the data 
rem folder gets installed whether the analysis or acquisition software is installed
mkdir files\data
XCOPY /E "%analysisdir%\data"  files\data 
rmdir /q /S  "%analysisdir%\data" 

IF %BuildLibrary% == false GOTO SKIPBUILDLIBRARY4

    rem Only unpack the sample folder if it exists.  The sample folder becomes part of the anaysis 
    rem installation. 
    if exist "%sampleszip%" %zipcomextract% %sampleszip% -o%samplesdir%

    rem Create a version manifest inside fo the Quant Library Folder so we have a version we can compare against 
    rem when running auto updater
    if exist "files\Quant Library\maxfullmanifest.xml" del /q /S  "files\Quant Library\maxfullmanifest.xml"

    %aubuilder% /executable "files\analysis\MAX Source Tester.exe"   /output "files\Quant Library\maxlibrarymanifest.xml"

:SKIPBUILDLIBRARY4


rem Now build the installers
IF %BuildMAX% == false GOTO SKIPMAXBUILD

    %WriteSuccess% Building %MaxInstaller%
    %nsiscom% /V2 /DINCLUDE_3RD_PARTY=yes /DVERSION="%buildversion%" %nisfile%
    if ERRORLEVEL 1 GOTO Error

:SKIPMAXBUILD

IF %BuildMAXUpdater% == false GOTO SKIPMAXUPDATER

    %WriteSuccess% Building %MaxUpdater%
    %nsiscom% /V2 /DVERSION="%buildversion%" %nisfile%
    if ERRORLEVEL 1 GOTO Error
    

:SKIPMAXUPDATER


IF %BuildLibrary% == false GOTO SKIPBUILDLIBRARY

    %WriteSuccess% Building %LibraryInstaller%
    %nsiscom% /V2 /DVERSION="%buildversion%" %nisqlfile%
    if ERRORLEVEL 1 GOTO Error

:SKIPBUILDLIBRARY

rem When done, move the installers into the build folder.
rmdir /Q /S %buildfolder%
mkdir %buildfolder%
rem Been having problems with the build folder not existingon move.  So give it a little time. 
TIMEOUT /T  1
move "MAX Analytical *.exe" %buildfolder%


Rem Copy any additional support files for auto updating into the build folder.  
copy AutoUpdate\* %buildfolder%

rem pause because we seems to have a timing issue with the file being scanned by virus software
timeout 10 > NUL 


rem makecert -n "CN=TempCA" -f -sv TempCAS.pvk TempCA.cer
rem pvk2pfx -pvk tempCAS.pvk -pfx tempcas.pfx -spc tempca.cer -pi abc

IF %BuildMAX% == false GOTO SKIPMAXBUILD2
    
    Rem Sign the installer
    %signtool% %MaxInstaller%
    if ERRORLEVEL 1 GOTO Error

	move %MaxInstaller% %FinalMaxInstaller%
	
    rem Now build the update manifest
    %aubuilder% /executable "files\analysis\MAX Source Tester.exe" /changelog changelog.html /installer  %FinalMaxInstaller% /output %buildfolder%\maxfullmanifest.xml
    if ERRORLEVEL 1 GOTO Error

:SKIPMAXBUILD2

IF %BuildMAXUpdater% == false GOTO SKIPMAXUPDATER2

    Rem Sign the installer

    %signtool% %MaxUpdater%
    if ERRORLEVEL 1 GOTO Error

	move %MaxUpdater% %FinalMaxUpdater%

    rem Now build the update manifest
    %aubuilder% /executable "files\analysis\MAX Source Tester.exe" /changelog changelog.html /installer %FinalMaxUpdater% /output %buildfolder%\maxupdatermanifest.xml
    if ERRORLEVEL 1 GOTO Error

:SKIPMAXUPDATER2

IF %BuildLibrary% == false GOTO SKIPBUILDLIBRARY2

    Rem Sign the installer
    %signtool% %LibraryInstaller%
    if ERRORLEVEL 1 GOTO Error

	move %LibraryInstaller% %FinalLibraryInstaller%

    rem Now build the update manifest
    %aubuilder% /executable "files\analysis\MAX Source Tester.exe" /changelog librarychangelog.html /installer %FinalLibraryInstaller% /output %buildfolder%\maxlibrarymanifest.xml
    if ERRORLEVEL 1 GOTO Error
:SKIPBUILDLIBRARY2


%WriteSuccess% Installer Build Completely Successfully
TIMEOUT /T  20

GOTO End

:Error
%WriteError% Error level is %ERRORLEVEL%
TIMEOUT /T  240

:End
