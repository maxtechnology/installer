Function RegisterRIC  

WriteRegStr HKCR ".ric" "" "RICFile"
WriteRegStr HKCR "RICFile" "" "Thermo Fisher Scientific Carbon Ladder Calibration"
WriteRegStr HKCR "RICFile\DefaultIcon" "" "$INSTDIR\bin\images\RICFile.ico"

WriteRegStr HKCR "RICFile\shell\open" "" "&Open"
WriteRegStr HKCR "RICFile\shell\open\command" "" '$INSTDIR\bin\CLCalibrationEditor.exe  /open "%1"'

FunctionEnd


Function Un.RegisterRIC  

  DeleteRegKey HKCR ".ric"
  DeleteRegKey HKCR RICFile
FunctionEnd