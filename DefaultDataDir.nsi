;
;  Initailizes the Data directory based on whether a registry key can be found for
;  a previous installation of the data directory. 
;

!define OLD_DATA_KEY_NAME "Prism MAX Data"
!define DEFAULT_DATA_FOLDER_NAME "MAX Data"

Function DefaultDataDir
	
    ClearErrors
    ReadRegStr $0 HKLM "Software\${DATA_KEY_NAME}" "DataDir"
	
    ${If} ${Errors}

	
		ClearErrors
		ReadRegStr $0 HKLM "Software\${OLD_DATA_KEY_NAME}" "DataDir"
	
		; We should be able to make reading the old key go away after 3.1
		${If} ${Errors}
		
		    StrCpy $0 $APPDATA 2
			StrCpy $DataDir "$0\${DEFAULT_DATA_FOLDER_NAME}"
			
		${Else}
		
			StrCpy $DataDir $0
		
		${EndIf}

	
	


    ${Else}
	
	    StrCpy $DataDir $0

    ${EndIf}
	

		
FunctionEnd