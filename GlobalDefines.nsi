;--------------------------------
; Global defines
  !ifndef VERSION
      !warning "VERSION was not defined"
      !define VERSION "Unknown"
  !endif

  !define COMPANY "Thermo Fisher Scientific"
  !define DATA_KEY_NAME "MAX Analytical Data"
  !define FULL_NAME "${APP_NAME} ${VERSION}"
  !define LOG_DIR "$TEMP"

  Unicode true
;--------------------------------
